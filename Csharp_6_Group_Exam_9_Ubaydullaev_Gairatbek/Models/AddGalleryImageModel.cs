﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Csharp_6_Group_Exam_9_Ubaydullaev_Gairatbek.Models
{
    public class AddGalleryImageModel
    {
        [Required]
        public int AdvertisementId { get; set; }

        public IFormFileCollection Images { get; set; }
    }
}
