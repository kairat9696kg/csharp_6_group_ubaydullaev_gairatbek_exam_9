﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Csharp_6_Group_Exam_9_Ubaydullaev_Gairatbek.Models
{
    public class CreateAdvertisementModel
    {


        [Required]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Содержание")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Контакты")]
        public int Contacts { get; set; }


        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Цена должна быть указана")]
        [Range(0, Double.PositiveInfinity, ErrorMessage = "Цена не может быть отрецательной")]
        public decimal Price { get; set; }

        [Display(Name = "Выберите главние Изображение")]
        public IFormFile Image { get; set; }


        [Display(Name = "Дата создания")]
        public DateTime DataPublished { get; set; }

        [Required]
        [Display(Name = "Категория")]
        public int CategoryId { get; set; }
        public SelectList CategoriesList { get; set; }

        [Display(Name = "Изображения")]
        public IFormFileCollection Images { get; set; }

    }
}
