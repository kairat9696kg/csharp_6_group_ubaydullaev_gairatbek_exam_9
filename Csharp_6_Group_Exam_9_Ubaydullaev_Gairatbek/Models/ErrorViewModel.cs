using System;

namespace Csharp_6_Group_Exam_9_Ubaydullaev_Gairatbek.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
