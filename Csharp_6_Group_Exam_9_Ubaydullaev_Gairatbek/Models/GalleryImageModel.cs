﻿namespace Csharp_6_Group_Exam_9_Ubaydullaev_Gairatbek.Models
{
    public class GalleryImageModel
    {
        public int AdvertisementId {get;set;}
        public string Name { get; set; }
        public byte[] Image { get; set; }
    }
}
