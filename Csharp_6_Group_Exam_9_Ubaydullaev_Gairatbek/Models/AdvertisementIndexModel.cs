﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Csharp_6_Group_Exam_9_Ubaydullaev_Gairatbek.Models
{
    public class AdvertisementIndexModel
    {
        public string SearchKey { get; set; }
        public decimal? PriceFrom { get; set; }
        public decimal? PriceTo { get; set; }
        public List<AdvertisementModel> advertisements { get; set; }
    }
}
