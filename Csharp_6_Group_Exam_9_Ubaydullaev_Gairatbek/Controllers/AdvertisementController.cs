﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Csharp_6_Group_Exam_9_Ubaydullaev_Gairatbek.Models;
using Csharp_6_Group_Exam_9_Ubaydullaev_Gairatbek.Services.Contracts;
using Exam_9_DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Csharp_6_Group_Exam_9_Ubaydullaev_Gairatbek.Controllers
{
    public class AdvertisementController : Controller
    {
        private readonly IAdvertisementService _advertisementService;
        private readonly UserManager<User> _userManager;

        public AdvertisementController(IAdvertisementService advertisementService, UserManager<User> userManager)
        {
            if (advertisementService == null)
                throw new ArgumentNullException(nameof(advertisementService));
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));

            _advertisementService = advertisementService;
            _userManager = userManager;

        }

        [Authorize]
        public async Task<IActionResult> myAdvertisements()
        {
            User currentUser = await _userManager.GetUserAsync(User);
            List<AdvertisementModel> model = _advertisementService.GetMyAdvertisement(currentUser).ToList();
            return View(model);
        }
        public IActionResult Index(AdvertisementIndexModel advertisement)
        {
            var model = _advertisementService.GetAllAdvertisement(advertisement);
            advertisement.advertisements = model;
            return View(advertisement);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = _advertisementService.GetCreateAdvertisementModel();
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateNew(CreateAdvertisementModel model)
        {
            User currentUser = await _userManager.GetUserAsync(User);

            _advertisementService.CreateAdvertisement(model, currentUser);

            return RedirectToAction("Index");
        }
    }
}
