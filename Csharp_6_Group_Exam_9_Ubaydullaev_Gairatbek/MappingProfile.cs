﻿using AutoMapper;
using Csharp_6_Group_Exam_9_Ubaydullaev_Gairatbek.Models;
using Exam_9_DAL.Entities;
using System;

namespace Csharp_6_Group_Exam_9_Ubaydullaev_Gairatbek
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateGalleryImageToModel();
            CreateAdvertisementToAdvertisementModelMap();
            CreateAdvertisementCreateModelToAdvertisement();
        }

        private void CreateGalleryImageToModel()
        {
            CreateMap<GalleryImage, GalleryImageModel>();
                
        }
       

        private void CreateAdvertisementToAdvertisementModelMap()
        {
            CreateMap<Advertisement, AdvertisementModel>()
                .ForMember(p => p.CategoryName,
                src => src.MapFrom(p => p.Category.Name))
                .ForMember(p => p.UserName,
                src => src.MapFrom(p => p.User.UserName))
                .ForMember(target => target.Images,
                    src => src.MapFrom(p => p.GalleryImages));
        }

        private void CreateAdvertisementCreateModelToAdvertisement()
        {
            CreateMap<CreateAdvertisementModel, Advertisement>()
                .ForMember(p=> p.DataPublished,
                src => src.MapFrom(p=> DateTime.Now))
                .ForMember(p => p.Image,
                src => src.Ignore());
        }
    }
}