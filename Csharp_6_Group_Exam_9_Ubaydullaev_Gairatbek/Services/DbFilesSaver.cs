﻿using Csharp_6_Group_Exam_9_Ubaydullaev_Gairatbek.Services.Contracts;
using Exam_9_DAL.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Csharp_6_Group_Exam_9_Ubaydullaev_Gairatbek.Services
{
    public class DbFilesSaver : IFileSaver
    {
        public void SaveFile(Advertisement advertisement, IFormFile formFile)
        {
            advertisement.Image = GetImageBytes(formFile);
        }

        public byte[] GetImageBytes(IFormFile formFile)
        {
            using (var binaryReader = new BinaryReader(formFile.OpenReadStream()))
            {
                return binaryReader.ReadBytes((int)formFile.Length);
            }
        }
    }
}
