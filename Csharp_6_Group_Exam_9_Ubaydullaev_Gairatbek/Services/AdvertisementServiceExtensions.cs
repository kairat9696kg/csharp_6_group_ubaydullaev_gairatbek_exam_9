﻿using Exam_9_DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Csharp_6_Group_Exam_9_Ubaydullaev_Gairatbek.Services
{
    public static class AdvertisementServiceExtensions
    {
        public static IEnumerable<Advertisement> BySearchKey(this IEnumerable<Advertisement> advertisements, string searchKey)
        {
            if (!string.IsNullOrWhiteSpace(searchKey))
                advertisements = advertisements.Where(r => r.Title.Contains(searchKey) || r.Description.Contains(searchKey));

            return advertisements;
        }

        public static IEnumerable<Advertisement> ByPriceFrom(this IEnumerable<Advertisement> advertisements, decimal? PriceFrom)
        {
            if (PriceFrom.HasValue)
                advertisements = advertisements.Where(r => r.Price >= PriceFrom.Value);

            return advertisements;
        }

        public static IEnumerable<Advertisement> ByPriceTo(this IEnumerable<Advertisement> advertisements, decimal? PriceTo)
        {
            if (PriceTo.HasValue)
                advertisements = advertisements.Where(r => r.Price <= PriceTo.Value);

            return advertisements;
        }
    }
}
