﻿using AutoMapper;
using AutoMapper.Configuration.Annotations;
using Csharp_6_Group_Exam_9_Ubaydullaev_Gairatbek.Models;
using Csharp_6_Group_Exam_9_Ubaydullaev_Gairatbek.Services.Contracts;
using Exam_9_DAL.Entities;
using Exam_9_DAL.Repositories.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Csharp_6_Group_Exam_9_Ubaydullaev_Gairatbek.Services
{
    public class AdvertisementService : IAdvertisementService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IFileSaver _fileSaver;

        public AdvertisementService(IUnitOfWorkFactory unitOfWorkFactory, IFileSaver fileSaver)
        {
            if (fileSaver == null)
                throw new ArgumentNullException(nameof(fileSaver));
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _unitOfWorkFactory = unitOfWorkFactory;
            _fileSaver = fileSaver;
        }


        public List<AdvertisementModel> GetAllAdvertisement(AdvertisementIndexModel advertisement)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var model = unitOfWork.Advertisement.GetAllWithAll();
                model = model
                    .BySearchKey(advertisement.SearchKey)
                    .ByPriceFrom(advertisement.PriceFrom)
                    .ByPriceTo(advertisement.PriceTo)
                    .OrderByDescending(p => p.DataPublished);

                return Mapper.Map<List<AdvertisementModel>>(model);
            }
        }

        public CreateAdvertisementModel GetCreateAdvertisementModel()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var Categories = unitOfWork.Category.GetAll().ToList();
                return new CreateAdvertisementModel()
                {
                    CategoriesList = new SelectList(Categories, nameof(Category.Id), nameof(Category.Name)),
                };
            }
        }

        public void CreateAdvertisement(CreateAdvertisementModel model, User currentUser)
        {

            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                Advertisement advertisement = Mapper.Map<Advertisement>(model);
                advertisement.UserId = currentUser.Id;

                if (model.Image != null)
                {
                    _fileSaver.SaveFile(advertisement, model.Image);
                }
                if (model.Images != null)
                {
                    UploadImages(model.Images, currentUser.Id);
                }


                unitOfWork.Advertisement.Create(advertisement);
            }
        }

        public void UploadImages(IFormFileCollection model, int userId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                foreach (var image in model)
                {
                    var galleryImage = new GalleryImage()
                    {
                        AdvertisementId = userId,
                        Name = image.FileName,
                        Image = _fileSaver.GetImageBytes(image)
                    };
                    unitOfWork.GalleryImage.Create(galleryImage);
                }
            }
        }

        

        public IEnumerable<AdvertisementModel> GetMyAdvertisement(User currentUser)
        {
            using(var unitOfWork = _unitOfWorkFactory.Create())
            {
                List<Advertisement> model = unitOfWork.Advertisement.GetByIdWithAll(currentUser.Id).ToList();
                
                return Mapper.Map<List<AdvertisementModel>>(model);
            }
        }
    }
}
