﻿using Csharp_6_Group_Exam_9_Ubaydullaev_Gairatbek.Models;
using Exam_9_DAL.Entities;
using System.Collections.Generic;

namespace Csharp_6_Group_Exam_9_Ubaydullaev_Gairatbek.Services.Contracts
{
    public interface IAdvertisementService
    {
        List<AdvertisementModel> GetAllAdvertisement(AdvertisementIndexModel advertisement);
        public CreateAdvertisementModel GetCreateAdvertisementModel();
        void CreateAdvertisement(CreateAdvertisementModel model, User currentUser);
        IEnumerable<AdvertisementModel> GetMyAdvertisement(User currentUser);
    }
}
