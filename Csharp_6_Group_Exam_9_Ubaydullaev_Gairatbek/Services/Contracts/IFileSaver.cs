﻿using Exam_9_DAL.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Csharp_6_Group_Exam_9_Ubaydullaev_Gairatbek.Services.Contracts
{
    public interface IFileSaver
    {
        void SaveFile(Advertisement advertisement, IFormFile formFile);
        public byte[] GetImageBytes(IFormFile formFile);
    }
}
