﻿using Exam_9_DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Exam_9_DAL.EntitiesConfiguration
{
    public class CategoryConfiguration : BaseEntityConfiguration<Category>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Category> builder)
        {
            builder
                .Property(b => b.Name)
                .HasMaxLength(100)
                .IsRequired();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Category> builder)
        {

            builder
                .HasMany(p => p.Advertisements)
                .WithOne(p => p.Category)
                .HasForeignKey(p => p.CategoryId)
                .IsRequired();
        }
    }


}
