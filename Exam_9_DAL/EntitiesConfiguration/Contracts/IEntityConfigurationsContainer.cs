﻿using Exam_9_DAL.Entities;

namespace Exam_9_DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Advertisement> AdvertisementConfiguration { get; }
        IEntityConfiguration<Category> CategoryConfiguration { get; }
        IEntityConfiguration<GalleryImage> GalleryImageConfiguration { get; }
    }
}
