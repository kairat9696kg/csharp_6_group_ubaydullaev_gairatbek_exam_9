﻿using Exam_9_DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Exam_9_DAL.EntitiesConfiguration
{
    public class AdvertisementConfiguration : BaseEntityConfiguration<Advertisement>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Advertisement> builder)
        {
            builder
                .Property(b => b.Title)
                .HasMaxLength(100)
                .IsRequired();

            builder
                .Property(b => b.Description)
                .HasMaxLength(1000);
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Advertisement> builder)
        {
            builder
                .HasOne(b => b.Category)
                .WithMany(b => b.Advertisements)
                .HasForeignKey(b => b.CategoryId)
                .IsRequired();

            builder
                .HasMany(p => p.GalleryImages)
                .WithOne(p => p.Advertisement)
                .HasForeignKey(p => p.AdvertisementId);

            builder
                .HasOne(p => p.User)
                .WithMany(p => p.Advertisements)
                .HasForeignKey(p => p.UserId)
                .IsRequired();
        }
    }


}
