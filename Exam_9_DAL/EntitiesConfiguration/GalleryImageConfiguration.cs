﻿using Exam_9_DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Exam_9_DAL.EntitiesConfiguration
{
    public class GalleryImageConfiguration : BaseEntityConfiguration<GalleryImage>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<GalleryImage> builder)
        {
            builder
                .Property(b => b.Name)
                .HasMaxLength(100)
                .IsRequired();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<GalleryImage> builder)
        {
            builder
                .HasOne(b => b.Advertisement)
                .WithMany(b => b.GalleryImages)
                .HasForeignKey(b => b.AdvertisementId);

        }
    }


}
