﻿using Exam_9_DAL.Entities;
using Exam_9_DAL.EntitiesConfiguration.Contracts;

namespace Exam_9_DAL.EntitiesConfiguration
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntityConfiguration<Advertisement> AdvertisementConfiguration { get; }
        public IEntityConfiguration<Category> CategoryConfiguration { get; }
        public IEntityConfiguration<GalleryImage> GalleryImageConfiguration { get; }

        public EntityConfigurationsContainer()
        {
            AdvertisementConfiguration = new AdvertisementConfiguration();
            CategoryConfiguration = new CategoryConfiguration();
            GalleryImageConfiguration = new GalleryImageConfiguration();
        }
    }


}
