﻿namespace Exam_9_DAL.Entities
{
    public interface IEntity
    {
        public int Id { get; set; }
    }
}
