﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace Exam_9_DAL.Entities
{
    public class User : IdentityUser<int>, IEntity
    {
        public ICollection<Advertisement> Advertisements { get; set; }
    }
}
