﻿using System;
using System.Collections.Generic;

namespace Exam_9_DAL.Entities
{
    public class Advertisement : IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Contacts { get; set; }
        public decimal Price { get; set; }
        public byte[]? Image { get; set; }
        public int UserId { get; set; }
        public DateTime DataPublished { get; set; }
        public User User { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public ICollection<GalleryImage> GalleryImages { get; set; }
    }
}
