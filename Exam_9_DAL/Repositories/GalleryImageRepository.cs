﻿using Exam_9_DAL.Entities;
using Exam_9_DAL.Repositories.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace Exam_9_DAL.Repositories
{
    public class GalleryImageRepository : Repository<GalleryImage>, IGalleryImageRepository
    {
        public GalleryImageRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.GalleryImages;
        }

        public IEnumerable<GalleryImage> GetByIdList(int id)
        {
            return entities
                .Where(e => e.AdvertisementId == id);
        }
    }
}
