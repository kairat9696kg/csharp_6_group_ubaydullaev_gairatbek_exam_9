﻿using Exam_9_DAL.Entities;
using Exam_9_DAL.Repositories.Contracts;

namespace Exam_9_DAL.Repositories
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Categories;
        }
    }
}
