﻿using Exam_9_DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam_9_DAL.Repositories.Contracts
{
    public interface IAdvertisementRepository : IRepository<Advertisement>
    {
        IEnumerable<Advertisement> GetAllWithAll();
        IEnumerable<Advertisement> GetByIdWithAll(int id);
    }
}
