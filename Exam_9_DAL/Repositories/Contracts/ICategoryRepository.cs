﻿using Exam_9_DAL.Entities;

namespace Exam_9_DAL.Repositories.Contracts
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }
}
