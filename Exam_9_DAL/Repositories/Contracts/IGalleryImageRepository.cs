﻿using Exam_9_DAL.Entities;
using System.Collections.Generic;

namespace Exam_9_DAL.Repositories.Contracts
{
    public interface IGalleryImageRepository : IRepository<GalleryImage>
    {
        IEnumerable<GalleryImage> GetByIdList(int id);
    }
}
