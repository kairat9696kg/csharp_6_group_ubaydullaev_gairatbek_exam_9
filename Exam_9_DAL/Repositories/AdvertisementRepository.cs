﻿using Exam_9_DAL.Entities;
using Exam_9_DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Exam_9_DAL.Repositories
{
    public class AdvertisementRepository : Repository<Advertisement>, IAdvertisementRepository
    {
        public AdvertisementRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Advertisements;
        }

        public IEnumerable<Advertisement> GetAllWithAll()
        {
            return entities
                .Include(p => p.Category)
                .Include(p => p.GalleryImages);
        }

        public IEnumerable<Advertisement> GetByIdWithAll(int id)
        {
            return entities
                .Include(p => p.Category)
                .Include(p => p.GalleryImages)
                .Where(p => p.UserId == id);
        }
    }
}
