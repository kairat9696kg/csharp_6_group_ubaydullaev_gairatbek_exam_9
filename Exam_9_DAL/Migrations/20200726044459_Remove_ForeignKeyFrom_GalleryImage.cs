﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Exam_9_DAL.Migrations
{
    public partial class Remove_ForeignKeyFrom_GalleryImage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GalleryImages",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Image = table.Column<byte[]>(nullable: true),
                    AdvertisementId = table.Column<int>(nullable: true)
                });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GalleryImages");
        }
    }
}
