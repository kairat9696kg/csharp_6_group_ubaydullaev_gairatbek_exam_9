﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Exam_9_DAL.Migrations
{
    public partial class addDateTimeToAdvertisement : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DataPublished",
                table: "Advertisements",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DataPublished",
                table: "Advertisements");
        }
    }
}
